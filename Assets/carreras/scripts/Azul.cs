﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rojo : MonoBehaviour {

	[SerializeField] float force= 0;
	bool playermov;


	[SerializeField] Text  chocolates;
	private int cont;
	[SerializeField] Text gamo;


	// Use this for initialization
	void Start () {




		
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			StartCoroutine (OneMoveTo(1.86f)); 
		}
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			StartCoroutine (OneMoveTo (4.27f));
		}
	}

	IEnumerator OneMoveTo(float tarX)
	{

		playermov = true;
		float distance = Vector2.Distance (transform.position, new Vector2 (tarX, 0));
		Vector2 tarPos = new Vector2 (tarX, transform.position.y);

		while (distance > 3f)
		{
			distance = Vector2.Distance (transform.position, new Vector2 (tarX, 0));

			if (transform.position.x > tarX )
			{
				transform.position = Vector2.MoveTowards (new Vector2 (transform.position.x, transform.position.y), tarPos, force * Time.deltaTime);
			}

			else if (transform.position.x < tarX )
			{
				transform.position = Vector2.MoveTowards (new Vector2 (transform.position.x, transform.position.y), tarPos, force * Time.deltaTime);
			}

			yield return new WaitForEndOfFrame ();
		}

		transform.position = new Vector3 (tarX, transform.position.y, transform.position.z);
		playermov = false;

	}



		void OnTriggerEnter2D (Collider2D other){

			if (other.gameObject.CompareTag ("rojo")) {
			other.gameObject.SetActive (false);

			cont = cont + 1;
			chocolates.text = "Score: " + cont.ToString();

			} 
			else if (other.gameObject.CompareTag ("blue")) {

				Destroy (gameObject);
			Destroy (gameObject);
			gamo.text = "GAME OVER";

			Time.timeScale = 0;
			}




	}
}
