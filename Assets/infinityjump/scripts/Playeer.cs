﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Playeer : MonoBehaviour {
	
	[SerializeField] float accumulatedForce = 0;
	private Rigidbody rb;
	bool jump = false;

	private int counter = -1;
	[SerializeField] Text count;
	//[SerializeField] Text gameover;


	private float rndX =  7f;
	private float rndY =  3f;

	[SerializeField] GameObject escalon;

	// Use this for initialization
	void Start () {
		
		rb = GetComponent<Rigidbody> ();
		//.enabled = false;

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Space) && !jump) {
			accumulatedForce += 20;


		}
		if (Input.GetKeyUp (KeyCode.Space)) {
			jump = true;
			Invoke ("resetjump",0.2f); 

		}

	}
	private void FixedUpdate(){
		if (jump) {
			rb.AddForce (new Vector3 (accumulatedForce * 0.3f, accumulatedForce, 0));
		}
	}

	void resetjump(){
		jump = false;
		accumulatedForce = 0;

	}

	void OnTriggerEnter (Collider other){

	
		if(other.gameObject.CompareTag("stair")){
				

				Instantiate (escalon, new Vector3 (rndX, rndY, 0), Quaternion.identity);

			rndY= rndY+4f;
			rndX= rndX+4f;
			}

		if (other.gameObject.CompareTag ("stair")) {

			counter = counter + 1;
			count.text = "Score: " + counter.ToString();

			
		}

		}

	void OnTriggerExit(Collider other){
		Destroy (other.gameObject);


	}


	void SetCountText(){
		count.text = "Score: " + counter.ToString ();

	
	}


	}

	



