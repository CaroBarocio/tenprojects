﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Step : MonoBehaviour {

	public GameObject[] itemToSpawn;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Spawn(){
		Instantiate(itemToSpawn [ Random.Range(0, itemToSpawn.Length)],
		new Vector3( Random.Range (0, 11), this.transform.position.y), Quaternion.identity);
	}
}
