﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player_Controller : MonoBehaviour
{

    public float speed;

    private Rigidbody rb;
	private int cubitos;
	[SerializeField] Text cubos;
	[SerializeField] Text win;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

		cubitos = 0;
		SetCountText();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
			cubitos = cubitos + 1;
			cubos.text = "cubitos: " + cubitos++;
        }

		if (cubitos >= 18) {
			win.text= "YOU WIN";

			Time.timeScale = 0;


		}
    }

	private void SetCountText(){
		
		cubos.text = "cubitos: "  +  cubitos++;
	}
}
