﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
	
	private Transform feo;
	[SerializeField] GameObject obstaculo;

	// Use this for initialization
	void Start () {

		feo = GetComponent<Transform> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		if (50 == Random.Range (0, 1000)) {

			Instantiate (obstaculo, new Vector3 (feo.transform.position.x, feo.transform.position.y, feo.transform.position.z), Quaternion.identity);
		}
	}
}
