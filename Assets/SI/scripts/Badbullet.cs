﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Badbullet : MonoBehaviour {


	[SerializeField] float speed;
	private Transform shot;


	void Start (){

		shot = transform;
	}

	void Update(){
		float move = speed * Time.deltaTime;
		shot.Translate (Vector3.down);
	}

	void OnTriggerEnter2D (Collider2D other){



		if (other.tag == "base") {
			Destroy (gameObject);
			Destroy (other.gameObject, 0.3f);
		}

		if (other.tag == "Player") {
			Destroy (gameObject);
			Destroy (other.gameObject, 0.3f);

		}
	}

	void OnBecameInvisible(){
		Destroy (gameObject);
	}
}
