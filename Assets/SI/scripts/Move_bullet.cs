﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Move_bullet : MonoBehaviour {

	[SerializeField] float speed;
	private Transform shot;

	[SerializeField] Text score;
	private int contador;


	void Start (){

		shot = transform;
		contador = 0;
	}

	void Update(){
		float move = speed * Time.deltaTime;
		shot.Translate (Vector3.up);
	}

	void OnTriggerEnter2D (Collider2D other){



		if (other.tag == "enemy") {

			IncreaseTextUIScore ();
			Destroy (gameObject);
			Destroy (other.gameObject, 0.3f);
		

		}

		if (other.tag == "base") {
			Destroy (gameObject);
		}
	}

	void IncreaseTextUIScore (){

		var textUIComp = GameObject.Find ("Text").GetComponent<Text> ();
		int score = int.Parse (textUIComp.text);
		score += 20;
		textUIComp.text = score.ToString ();
	}

	void OnBecameInvisible(){
		Destroy (gameObject);
	}
}
	


