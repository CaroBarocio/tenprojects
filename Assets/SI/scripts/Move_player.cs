﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_player : MonoBehaviour {
	
	//private Rigidbody2D rb;
	private float nextfire;
	[SerializeField] GameObject bala;
	public Transform shotspawn;
	public float fireRate;
	private float max, min;

	[SerializeField] float speed;



	// Use this for initialization
	void Start () {

		//rb = GetComponent<Rigidbody2D> ();
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {



		if (Input.GetKey (KeyCode.A) && transform.position.x > -8.6f) {
			transform.Translate (Vector2.left * Time.deltaTime * speed);

		}

		else if (Input.GetKey (KeyCode.D) && transform.position.x <8.6f){
			transform.Translate (Vector2.right * Time.deltaTime * speed);

		}
	
	}
	void Update() {
		
		if (Input.GetMouseButtonDown(0)) {
			
			nextfire = Time.time + fireRate;

			Instantiate (bala, shotspawn.position, shotspawn.rotation);
		}
			
			
		
	}

}