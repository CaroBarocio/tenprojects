﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public float speed = 0.5f;
	private Rigidbody2D rb;
	[SerializeField] GameObject badbullet;
	[SerializeField] float minFireRate = 1.0f;
	[SerializeField] float maxFireRate = 3.0f;
	[SerializeField] float baseFireWaitRate = 3.0f;

	// Use this for initialization
	void Start () {

		rb = GetComponent<Rigidbody2D> ();
		rb.velocity = new Vector2 (1, 0) * speed;
		baseFireWaitRate = baseFireWaitRate + Random.Range (minFireRate, maxFireRate);
		
	}

	void Turn(int direction){

		Vector2 newVelocity = rb.velocity;
		newVelocity.x = speed *direction;
		rb.velocity = newVelocity; 
	}

	void MoveDown(){
		Vector2 position = transform.position;
		position.y -= 0.5f;
		transform.position = position;
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.name == "LeftWall") {

			Debug.Log ("left collision");
			Turn (1);
			MoveDown ();
		}
		if (other.gameObject.name == "RighttWall") {

			Debug.Log ("right collision");
			Turn (-1);
			MoveDown ();
	}
		if (other.gameObject.name == "bullet") {
			Destroy (gameObject);
		}

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (Time.time > baseFireWaitRate) {
			baseFireWaitRate = baseFireWaitRate + Random.Range (minFireRate, maxFireRate);

			Instantiate (badbullet, transform.position, Quaternion.identity);
		}
		
	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.gameObject.name == "jugador1") {
			Destroy (other.gameObject);
		}
	}
}
