using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player : MonoBehaviour { 


    public float velocidad;
    private Rigidbody2D rb2d;
    private int contador;
    public Text countText;


	// Use this for initialization
	void Start () {

    rb2d = GetComponent<Rigidbody2D>();
        contador = 0;
        SetCountText();
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

    float movehorizontal = Input.GetAxis("Horizontal");

    float movevertical = Input.GetAxis("Vertical");

    Vector2 movement = new Vector2(movehorizontal, movevertical);

    rb2d.AddForce(movement * velocidad);

}
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("recoger"))
        {
            other.gameObject.SetActive(false);
            contador = contador + 1;
            countText.text = "Cajitas: " + contador.ToString();
        }
    }

    void SetCountText() {
        countText.text = "cajitas: " + contador.ToString();
    }
        
    
}
