﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tubos : MonoBehaviour {

	private Transform tubos;
	[SerializeField] GameObject columns;
	// Use this for initialization
	void Start () {

		tubos = GetComponent<Transform> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		if (50 == Random.Range (0,300)) {

			Instantiate (columns, new Vector3 (tubos.transform.position.x, 0, 0), Quaternion.identity);
		}

		
	}
}

