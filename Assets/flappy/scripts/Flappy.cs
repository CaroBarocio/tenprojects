﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flappy : MonoBehaviour {

	Vector3 velocidad = Vector3.zero;

	[SerializeField] Vector3 gravedad;
	[SerializeField] Vector3 velocidadAleteo;

	bool aleteo = false;
	[SerializeField] float velocidadMax;
	[SerializeField] Text bye;

	private int saltos;
	[SerializeField] Text salcont;

	void Start (){
		saltos = 0;



	}

	void Update (){

		if (Input.GetKeyDown (KeyCode.Space) || Input.GetMouseButtonDown (0)) {
			aleteo = true;
		}
		
	}

	void FixedUpdate(){

		velocidad += gravedad * Time.deltaTime;

		if (aleteo == true) {
			aleteo = false;
			velocidad.y = velocidadAleteo.y;
		}

		transform.position += velocidad * Time.deltaTime;
		float angulo = 0;
		if (velocidad.y >= 0) {

			angulo = Mathf.Lerp (0, 25, velocidad.y / velocidadMax);
		} 
		else {
			angulo = Mathf.Lerp (0, -75, -velocidad.y / velocidadMax);
		}

		transform.rotation = Quaternion.Euler (0, 0, angulo);




	}
	void OnTriggerEnter2D (Collider2D other){


		if(other.gameObject.CompareTag("topa")){
			Destroy(gameObject);
			bye.text = "GAME OVER";

			Time.timeScale = 0;
		}

		else if (other.gameObject.CompareTag("cuenta")){
			other.gameObject.SetActive(false);
			saltos = saltos + 1;
			salcont.text = "Score: : " + saltos.ToString();
		}

	}




	

}